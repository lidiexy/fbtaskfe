<div class="item col-md-4 col-sm-6 col-xs-12">
    <div class="my-thumb base-container">
        <div class="bg-center text-center my-thumb-body" style="background-image: url('<?= $model->source; ?>')">
            <div class="row row-table">
                <div class="col-xs-12 text-white">
                    <img src="images/vanderbilt-waterstamp.png" alt="Vanderbilt" class="img-thumbnail img-circle thumb128">
                    <h3 class="non-margin">&nbsp;</h3>
                    <p class="non-margin">
                        &nbsp;
                    </p>
                </div>
            </div>
        </div>
        <div class="my-thumb-body text-center bg-vand-color">
            <div class="row row-table">
                <div class="col-xs-6 text-white text-2x">
                    <?= \yii\helpers\Html::a('View Detail', ['site/detail', 'id'=>$model->id]); ?>
                </div>
                <div class="col-xs-6 text-white text-2x">
                    <h4 class="non-margin"><?=$model->likes;?></h4>
                    <p class="non-margin text-muted">
                        <small>Likes</small>
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>

