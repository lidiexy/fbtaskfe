<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use kop\y2sp\ScrollPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchGallery */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vanderbilt Facebook Gallery';
?>
<div class="site-index">
    <div class="col-xs-12 gallery">
        <div class="page-header">
            <h1>Vanderbilt University <small>photos from Facebook page</small></h1>
        </div>
        <div class="list-view">
            <?php
            echo ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'item'],
                'itemView' => '_item_view',
                'summary' => '',
                'layout' => "{sorter}\n{items}\n{pager}",
                'pager' => [
                    'class' => ScrollPager::className(),
                    'negativeMargin' => '200',
                    'triggerText' => 'Load More Photos',
                    'triggerOffset' => 3,
                    'noneLeftText' => '',
                ],
                'sorter' => [
                    'attributes' => ['likes','created_time'],
                ]
            ]);
            ?>
        </div>
    </div>
</div>
