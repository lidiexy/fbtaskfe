<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="jumbotron">
        <img src="images/vanderbilt-logo.jpg" class="img-circle" alt="Vanderbilt University" title="Vanderbilt University"/>
        <h1>Front-End Side App!</h1>
        <p>
            This Web Application was made by Lidiexy Alonso <a href="mailto:lidiexy@gmail.com">lidiexy@gmail.com</a>.
            The main goal of this application is Manage the Photo Gallery for Vanderbilt University.
            This project is the Front-End side of the programming task created by Jason Tan and colleagues.
        </p>
    </div>
</div>