<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Gallery */

$this->title = Html::encode($model->name);
$this->params['breadcrumbs'][] = $model->id;
?>
<div class="gallery-view">
    <div class="page-header">
        <h1>Vanderbilt Photo Id <small><?= $model->id ?></small></h1>
    </div>

    <div class="bg-white text-center">
        <div class="row row-table">
            <div class="col-xs-12">
                <img class="img-resp" src="<?= $model->source; ?>" alt="<?= Html::encode($model->name); ?>" title="<?= Html::encode($model->name); ?>"/>
            </div>
        </div>
    </div>
    <div class="my-thumb-body text-center bg-info-color">
        <div class="row row-table">
            <div class="col-xs-12 text-white">
                <?= Html::encode($model->name); ?>
            </div>
        </div>
    </div>

    <div class="my-thumb-body text-center bg-vand-color">
        <div class="row row-table">
            <div class="col-xs-6 text-white text-2x">
                <h4 class="non-margin"><?=$model->likes;?></h4>
                <p class="non-margin text-muted">
                    <small>Likes</small>
                </p>
            </div>
            <div class="col-xs-6 text-white text-2x">
                <?php
                $date = DateTime::createFromFormat('Y-m-d H:i:s', $model->created_time);
                ?>
                <h4 class="non-margin"><?= $date->format('F d, Y h:ia');?></h4>
                <p class="non-margin text-muted">
                    <small>Uploaded</small>
                </p>
            </div>
        </div>
    </div>

    <div class="my-thumb-body text-center bg-link-color">
        <div class="row row-table">
            <div class="col-xs-12">
                <span class="glyphicon glyphicon-link"></span> <?= $model->source ?>
            </div>
        </div>
    </div>

    <div class="row row-table">
        <p>&nbsp;</p>
        <?= Html::a('Back to the list', $lastUrl, ['class' => 'btn btn-default']); ?>
    </div>

</div>