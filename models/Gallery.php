<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gallery".
 *
 * @property integer $id
 * @property string $name
 * @property string $created_time
 * @property integer $likes
 * @property string $source
 *
 * @author Lidiexy Alonso <lidiexy@gmail.com>
 * @link https://bitbucket.org/lidiexy/fbtaskbe/
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(){
        return 'gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules(){
        return [
            [['id', 'created_time', 'source'], 'required'],
            [['id', 'likes'], 'integer'],
            [['created_time'], 'safe'],
            [['source'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_time' => 'Created Time',
            'likes' => 'Likes',
            'source' => 'Source',
        ];
    }
}
